# 360 video example

This demo uses [AFrame](https://aframe.io) to display 360 video.

[DEMO](https://tournamentdepot.com/examples/3d-video-demo)

## Setup

Have npm or yarn installed

`yarn start` or `npm run start`

navigate to [http://127.0.0.1:8081/](http://127.0.0.1:8081/)

## Links
[Example of image galary ui for changing scene](https://aframe.io/docs/1.5.0/guides/building-a-360-image-gallery.html)
